define(function (require, exports, module) {

    'use strict';

    var $ = require('jquery');

    /**
     * @param {{getPreference: function}} widget
     * @constructor
     */
    function App(widget) {
        this.widget = widget;
        this.$widget = $(widget.body);
    }

    /**
     * Kick off widget initialization, data loading, rendering, etc.
     */
    App.prototype.init = function () {
        // console.log('widget init');
    };

    /**
     * Remove event listeners, clean up DOM, etc. to avoid memory leaks.
     */
    App.prototype.destroy = function () {
        // console.log('widget destroy');
    };

     /**
     * Store variable to be able to access it in exports.destroy function.
     */
    var app;
    /**
     * Public API.
     * @type {App}
     */
    exports.App = App;

    /**
     * Widget initialization hook, called by portal manager.
     * @param {object} widget
     * @returns {App}
     */
    exports.run = function (widget) {
        app = new App(widget);
        app.init();
        return app;
    };

    /**
     * Widget destruction hook, called by portal manager.
     */
    exports.destroy = function() {
        app.destroy();
        app = null;
    };

});
