# ${widget.name}

${widget.description}

## Information

| name                  | version           | bundle           |
| ----------------------|:-----------------:| ----------------:|
| ${widget.name}        | ${widget.version} |                   |


## Dependencies

* jquery

## Dev Dependencies

* **config 2.x**  - *used for requirejs.conf*
* **base 2.x**  - *used for requireWidget function*


## Preferences
- List of widget preferences

## Events
- List of event the widget publishes/subscribes

## Custom Components
- list of widget custom components (if any) 

## Requirements

-list of requirements


## Develop Standalone

```bash
bower i && bblp start
```


## Tests

```bash
$ bblp test
```

with watch flag

```bash
bblp test -w
```

## Build

```bash
$ bblp build
```

