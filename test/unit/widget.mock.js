/**
 * Mock necessary widget methods.
 * @constructor
 */


function Widget () {

    this.getPreference = function (name) {

        var prefs = {
            somePreference: '123'
        };

        return prefs[name] || '';
    };

    // Need to mock widget.body HTML, use real index.html widget content
    // To get only body tag contents: widgetBody.match(/<body[\s\S]*?>([\s\S]*?)<\/body>/)[1];
    var widgetBody = require('html!../../index.html');
    this.body = '<div>' + widgetBody + '</div>';

}

module.exports = new Widget();
