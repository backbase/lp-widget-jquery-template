/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : main.spec.js
 *  Description:
 *  ----------------------------------------------------------------
 */


// Load jquery matchers and mocks https://www.npmjs.com/package/jasmine-jquery
window.jQuery = window.$ = require('jquery');
require('jasmine-jquery');

var widget = require('../../scripts/main');
var __WIDGET__ = require('./widget.mock');

/**
 * Widget unit tests
 */
describe('Function based widget testing suit', function () {

    /**
     * Main module.
     */
    describe('${widget.name} testing suite', function () {

        it('should be an object', function () {
            expect(widget).toBeObject();
        });

        it('should define init method', function () {
            expect(widget.run).toBeFunction();
        });

        it('should define destroy method', function () {
            expect(widget.destroy).toBeFunction();
        });

        describe('App constructor', function () {

            var instance;
            var App = widget.App;

            beforeEach(function () {
                instance = new App(__WIDGET__);
            });

            it('should be a function', function () {
                expect(instance instanceof App).toBeTruthy();
            });

        });

    });

});

